package net.juzabel.socialimagelist

import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.presentation.ui.presenters.TagListPresenter
import net.juzabel.socialimagelist.presentation.ui.views.fragments.TagListFragment
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Example local unit test, which will execute on the development machine (host).

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class TagListUnitTest {


    @Mock
    internal var presenter: TagListPresenter? = null

    var view : TagListFragment? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        view = TagListFragment()
        view!!.tagListPresenter = presenter!!

        `when`(presenter!!.getList()).then {
            view!!.queryDone(ArrayList<Tag>())
        }

    }

    @Test
    fun testDisplayCalled() {
        assert(view!!.hasTagList())
    }
}