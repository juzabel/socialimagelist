package net.juzabel.socialimagelist.domain.repository

import io.reactivex.Observable
import net.juzabel.socialimagelist.domain.Image

/**
 * Created by juzabel on 22/7/17.
 */
interface ImageListRepository {
    fun getListImages(query : String, page : Int, items : Int) : Observable<List<Image>>
}