package net.juzabel.socialimagelist.domain.exception

/**
 * Created by juzabel on 22/7/17.
 */
interface ErrorBundle {
    fun getException(): Exception

    fun getErrorMessage(): String
}