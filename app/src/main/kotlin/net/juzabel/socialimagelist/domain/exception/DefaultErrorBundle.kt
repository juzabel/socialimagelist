package net.juzabel.socialimagelist.domain.exception

/**
 * Created by juzabel on 22/7/17.
 */
class DefaultErrorBundle constructor(private val exception: Exception) {

    private val DEFAULT_EXCEPTION_STRING = "Unknown Exception"

    fun getException() = exception

    fun getErrorMessage(): String = if (exception != null) exception.message!! else DEFAULT_EXCEPTION_STRING

}