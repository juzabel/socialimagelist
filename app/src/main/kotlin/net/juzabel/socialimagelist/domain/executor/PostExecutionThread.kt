package net.juzabel.socialimagelist.domain.executor

import io.reactivex.Scheduler


/**
 * Created by juzabel on 21/7/17.
 */
interface PostExecutionThread {
    val scheduler : Scheduler
}