package net.juzabel.socialimagelist.domain.interactors

import io.reactivex.Observable
import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.domain.executor.ExecutionThread
import net.juzabel.socialimagelist.domain.executor.PostExecutionThread
import net.juzabel.socialimagelist.domain.repository.TagListRepository
import javax.inject.Inject

/**
 * Created by juzabel on 23/7/17.
 */
class TagInteractor
@Inject constructor(threadExecutor: ExecutionThread, postExecutionThread: PostExecutionThread,
                                   private val tagListRepository: TagListRepository) : UseCase<Tag>(threadExecutor, postExecutionThread){

    private var tag : String? = null


    fun setParameters(tag : String) {
        this.tag = tag
    }

    override fun buildUseCaseObservable(): Observable<Tag> {

        tag = if(tag == null) "" else tag

        return this.tagListRepository.tag(tag!!)
    }


}