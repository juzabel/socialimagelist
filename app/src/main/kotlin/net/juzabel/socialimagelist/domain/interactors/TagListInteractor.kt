package net.juzabel.socialimagelist.domain.interactors

import io.reactivex.Observable
import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.domain.executor.ExecutionThread
import net.juzabel.socialimagelist.domain.executor.PostExecutionThread
import net.juzabel.socialimagelist.domain.repository.TagListRepository
import javax.inject.Inject

/**
 * Created by juzabel on 24/7/17.
 */
open class TagListInteractor @Inject constructor(threadExecutor: ExecutionThread, postExecutionThread: PostExecutionThread,
                                            private val tagListRepository: TagListRepository) : UseCase<List<Tag>>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(): Observable<List<Tag>> {
        return this.tagListRepository.getTagList()
    }


}