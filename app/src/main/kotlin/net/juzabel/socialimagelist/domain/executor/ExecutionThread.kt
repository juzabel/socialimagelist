package net.juzabel.socialimagelist.domain.executor

import java.util.concurrent.Executor

/**
 * Created by juzabel on 22/7/17.
 */
interface ExecutionThread : Executor