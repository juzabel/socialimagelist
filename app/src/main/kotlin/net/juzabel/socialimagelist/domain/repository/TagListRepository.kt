package net.juzabel.socialimagelist.domain.repository

import io.reactivex.Observable
import net.juzabel.socialimagelist.domain.Tag

/**
 * Created by juzabel on 22/7/17.
 */
interface TagListRepository {
    fun getTagList() : Observable<List<Tag>>

    fun tag(tag : String) : Observable<Tag>
}