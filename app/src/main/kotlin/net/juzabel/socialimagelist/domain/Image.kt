package net.juzabel.socialimagelist.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by juzabel on 22/7/17.
 */
open class Image(val purl : String, val psocialNetwork : String) : Parcelable {
    open var url = purl

    open var socialNetwork = psocialNetwork

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Image> = object : Parcelable.Creator<Image> {
            override fun createFromParcel(source: Parcel): Image = Image(source)
            override fun newArray(size: Int): Array<Image?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
    source.readString(),
    source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(purl)
        dest.writeString(psocialNetwork)
    }
}