package net.juzabel.socialimagelist.domain.interactors

import io.reactivex.Observable
import net.juzabel.socialimagelist.domain.Image
import net.juzabel.socialimagelist.domain.executor.ExecutionThread
import net.juzabel.socialimagelist.domain.executor.PostExecutionThread
import net.juzabel.socialimagelist.domain.repository.ImageListRepository
import javax.inject.Inject

/**
 * Created by juzabel on 23/7/17.
 */
class ImageListInteractor
@Inject constructor(threadExecutor: ExecutionThread, postExecutionThread: PostExecutionThread,
                                   private val imageListRepository: ImageListRepository) : UseCase<List<Image>>(threadExecutor, postExecutionThread){

    private var page : Int? = null
    private var perPage : Int? = null
    private var query : String? = null


    fun setParameters(query : String, page: Int, perPage: Int) {
        this.query = query
        this.page = page
        this.perPage = perPage
    }

    override fun buildUseCaseObservable(): Observable<List<Image>> {

        page = if(page == null) 1 else page
        perPage = if(perPage == null) 20 else perPage
        query = if(query == null) "all" else query


        return this.imageListRepository.getListImages(query!!, page!!, perPage!!)
    }


}