package net.juzabel.socialimagelist.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by juzabel on 22/7/17.
 */
data class Tag(val id : Long, val name : String) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Tag> = object : Parcelable.Creator<Tag> {
            override fun createFromParcel(source: Parcel): Tag = Tag(source)
            override fun newArray(size: Int): Array<Tag?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
    source.readLong(),
    source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(name)
    }
}