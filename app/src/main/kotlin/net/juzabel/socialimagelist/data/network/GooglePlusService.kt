package net.juzabel.socialimagelist.data.network

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.GPlusEntity
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by juzabel on 23/7/17.
 */

//https://www.googleapis.com/plus/v1/people?query=android&key=%20AIzaSyA6pEYr-UEAyOKwNRlP7xaMYk7pwVQxNys
interface GooglePlusService {

    companion object {
        val API_KEY : String  = "AIzaSyARoCBpNwIN3xb4Sbd4U-OUVQdZuO7LkF8"
    }


    @GET("v1/people")
    fun getListPeople(@Query("query") query : String, @Query("key") apikey : String) : Observable<GPlusEntity>


}