package net.juzabel.socialimagelist.data.repository

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.mapper.ImageMapper
import net.juzabel.socialimagelist.data.repository.datasource.GplusDataFactory
import net.juzabel.socialimagelist.data.repository.datasource.InstagramDataFactory
import net.juzabel.socialimagelist.domain.Image
import net.juzabel.socialimagelist.domain.repository.ImageListRepository
import javax.inject.Inject

/**
 * Created by juzabel on 23/7/17.
 */
class ImageListDataRepository
@Inject constructor(private val mapper: ImageMapper, private val dataFactory: GplusDataFactory, private val instagramDataFactory: InstagramDataFactory) : ImageListRepository {
    override fun getListImages(query: String, page: Int, items: Int): Observable<List<Image>> {
        return Observable.concat(

                dataFactory.createCloudDataStore()
                .personList(query, page, items)
                .map({ mapper.transform(it) })

                ,

                instagramDataFactory.createCloudDataStore()
                        .imageList(query, page, items).map { mapper.transform(it) }

                )
    }
}