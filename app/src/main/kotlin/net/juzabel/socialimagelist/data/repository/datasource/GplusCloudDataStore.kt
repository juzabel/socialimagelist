package net.juzabel.socialimagelist.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.GPlusEntity
import net.juzabel.socialimagelist.data.network.RestService

/**
 * Created by juzabel on 23/7/17.
 */
class GplusCloudDataStore(private val restService: RestService) : GPlusDataStore {

    override fun personList(query : String, page: Int, numItems: Int): Observable<GPlusEntity> =
            restService.getListPeople(query)


}