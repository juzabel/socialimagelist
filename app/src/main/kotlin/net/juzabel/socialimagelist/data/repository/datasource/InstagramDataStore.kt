package net.juzabel.socialimagelist.data.repository.datasource

import io.reactivex.Observable

/**
 * Created by juzabel on 23/7/17.
 */
interface InstagramDataStore {
    fun imageList(query : String, page: Int, numItems: Int): Observable<String>
    fun auth()
}