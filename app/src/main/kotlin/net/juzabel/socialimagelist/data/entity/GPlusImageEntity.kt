package net.juzabel.socialimagelist.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by juzabel on 23/7/17.
 */
data class GPlusImageEntity(
        @SerializedName("url")
        @Expose
        var url : String,

        @SerializedName("type")
        @Expose
        var type : String
)