package net.juzabel.socialimagelist.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.TagEntity

/**
 * Created by juzabel on 23/7/17.
 */
interface TagDataStore {
    fun tagList(): Observable<List<TagEntity>>
    fun add(tag : String) : Observable<TagEntity>
}