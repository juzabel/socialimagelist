package net.juzabel.socialimagelist.data.repository.datasource

import com.raizlabs.android.dbflow.rx2.language.RXSQLite
import com.raizlabs.android.dbflow.sql.language.SQLite
import io.reactivex.Observable
import net.juzabel.socialimagelist.data.cache.DbHelper
import net.juzabel.socialimagelist.data.entity.TagEntity


/**
 * Created by juzabel on 24/7/17.
 */
class TagDBDataStore(private val dbHelper: DbHelper) : TagDataStore {
    override fun add(tag: String): Observable<TagEntity> {

        var tagEntity: TagEntity = TagEntity()
        tagEntity.tag = tag

        return tagEntity.insert().map {
            tagEntity
        }.toObservable()
    }

    override fun tagList(): Observable<List<TagEntity>> {
        return RXSQLite.rx<TagEntity>(
                SQLite.select().from<TagEntity>(TagEntity::class.java!!))
                .queryList()
                .toObservable()
    }
}