package net.juzabel.socialimagelist.data.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by juzabel on 23/7/17.
 */
data class UserCodeEntity(

        @SerializedName("device_code")
        val deviceCode : String,

        @SerializedName("user_code")
        val userCode : String,

        @SerializedName("verification_url")
        val verificationUrl : String,

        @SerializedName("expires_in")
        val expiresIn : Long,

        val interval : Int

)