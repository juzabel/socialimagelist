package net.juzabel.socialimagelist.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.GPlusEntity

/**
 * Created by juzabel on 23/7/17.
 */
interface GPlusDataStore {
    fun personList(query : String, page: Int, numItems: Int): Observable<GPlusEntity>
}