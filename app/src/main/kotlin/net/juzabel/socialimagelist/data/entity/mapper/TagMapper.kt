package net.juzabel.socialimagelist.data.entity.mapper

import net.juzabel.socialimagelist.data.entity.TagEntity
import net.juzabel.socialimagelist.domain.Tag
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 24/7/17.
 */
@Singleton
class TagMapper @Inject constructor() {

    fun transform(tagEntity: TagEntity): Tag {

        var id: Long = tagEntity.id
        var tag: String = tagEntity.tag

        var retTag: Tag = Tag(id, tag)

        return retTag

    }

    fun transform(tagEntityList : List<TagEntity>) : List<Tag> {

        var retTagList : ArrayList<Tag> = ArrayList<Tag>()

        for(tagEntity in tagEntityList){
            retTagList.add(transform(tagEntity))
        }

        return retTagList
    }


}