package net.juzabel.socialimagelist.data.repository

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.mapper.TagMapper
import net.juzabel.socialimagelist.data.repository.datasource.TagDataFactory
import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.domain.repository.TagListRepository
import javax.inject.Inject

/**
 * Created by juzabel on 24/7/17.
 */
class TagDataRepository
@Inject constructor(private val mapper: TagMapper, private val dataFactory: TagDataFactory) : TagListRepository {
    override fun getTagList(): Observable<List<Tag>> {
       return dataFactory.createDBDataStore().tagList().map({mapper.transform(it)})
    }

    override fun tag(tag: String): Observable<Tag> {
        return dataFactory.createDBDataStore().add(tag).map({mapper.transform(it)})
    }

}