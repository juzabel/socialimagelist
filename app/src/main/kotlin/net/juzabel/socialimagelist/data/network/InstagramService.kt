package net.juzabel.socialimagelist.data.network

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.InstagramToken
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by juzabel on 25/7/17.
 */

interface InstagramService {

    companion object {
        var clientId = "33088769d1124e84afeb1f3e0b2977fe"
        var secretId = "fced136dc6c84597a4391e7755f4bb52"
        var code = "a201634a6fa14bbc8039aa0d4dbf69e8"
    }

    @GET("oauth/authorize")
    fun getCode(@Query("client_id") clientId: String, @Query("redirect_uri") redirectUri: String,
                @Query("response_type") responseType : String) : Observable<Response<String>>

    @FormUrlEncoded
    @POST("oauth/access_token")
    fun authenticate(@Field("client_id") clientId : String,
                     @Field("client_secret") clientSecret : String,
                     @Field("grant_type") grantType : String,
                     @Field("redirect_uri") redirectUri : String,
                     @Field("code") code : String
                     ) : Observable<InstagramToken>

    @GET("v1/tags/{tag_name}/media/recent")
    fun getImages(@Path("tag_name") query : String, @Query("access_token") accessToken : String) : Observable<String>

}