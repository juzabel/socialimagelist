package net.juzabel.socialimagelist.data.entity

/**
 * Created by juzabel on 24/7/17.
 */
data class GPlusEntity(
        var kind: String,
        var etag: String,
        var selflink: String,
        var title: String,
        var nextPageToken: String,
        var items : List<GPlusPersonEntity>
)