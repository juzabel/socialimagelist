package net.juzabel.socialimagelist.data.network

import com.google.gson.Gson
import io.reactivex.Observable
import net.juzabel.socialimagelist.data.entity.GPlusEntity
import net.juzabel.socialimagelist.data.entity.GPlusPersonEntity
import net.juzabel.socialimagelist.data.entity.InstagramToken
import net.juzabel.socialimagelist.presentation.di.scopes.FragmentScope
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 23/7/17.
 */
@Singleton
class RestService @Inject constructor() {

    private val googlePlusService: GooglePlusService
    private val instagramService: InstagramService

    init {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder: OkHttpClient.Builder = OkHttpClient.Builder().addInterceptor(interceptor)

        val retrofitGplus: Retrofit = Retrofit.Builder().baseUrl(GPLUS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build()).build()
        googlePlusService = retrofitGplus.create(GooglePlusService::class.java)

        val retrofitInstagram: Retrofit = Retrofit.Builder().baseUrl(INSTAGRAM_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build()).build()

        instagramService = retrofitInstagram.create(InstagramService::class.java)


    }

    fun getListPeople(query: String): Observable<GPlusEntity> {
        return googlePlusService.getListPeople(query, GooglePlusService.API_KEY)
    }


    companion object {
        val GPLUS_BASE_URL = "https://www.googleapis.com/plus/"
        val INSTAGRAM_BASE_URL = "https://api.instagram.com"
        val INSTAGRAM_REDIRECT_URL = "http://redirect.juzabel.net"
    }

    fun authInstagram(): Observable<InstagramToken> {


        return instagramService.authenticate(InstagramService.clientId,
                InstagramService.secretId,
                "authorization_code",
                INSTAGRAM_REDIRECT_URL,
                InstagramService.code)


    }

    fun getInstagramImageList(query: String, token: String): Observable<String> {

        return instagramService.getImages(query, token)
    }

}