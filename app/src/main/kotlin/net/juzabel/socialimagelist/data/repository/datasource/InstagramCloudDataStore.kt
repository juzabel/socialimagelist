package net.juzabel.socialimagelist.data.repository.datasource

import io.reactivex.Observable
import net.juzabel.socialimagelist.data.network.RestService

/**
 * Created by juzabel on 23/7/17.
 */
class InstagramCloudDataStore(private val restService: RestService) : InstagramDataStore {

    private var token: String? = null
    private var code : String? = null

    override fun auth() {

    }


    override fun imageList(query: String, page: Int, numItems: Int): Observable<String> {

        if (token == null) {
            return restService.authInstagram().flatMap {
                token = it.accessToken
                restService.getInstagramImageList(query, it.accessToken)
            }
        } else {
            return restService.getInstagramImageList(query, token!!)
        }
    }


}