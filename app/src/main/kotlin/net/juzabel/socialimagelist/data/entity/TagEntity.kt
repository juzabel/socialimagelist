package net.juzabel.socialimagelist.data.entity

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.rx2.structure.BaseRXModel
import net.juzabel.socialimagelist.data.cache.DbHelper

/**
 * Created by juzabel on 23/7/17.
 */
@Table(name = "items", database = DbHelper::class)
class TagEntity : BaseRXModel() {

    @PrimaryKey(autoincrement = true)
    @Column(name = "id")
    var id: Long = 0

    @Column(name = "tag")
    var tag : String = ""
}