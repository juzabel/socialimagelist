package net.juzabel.socialimagelist.data.cache

import com.raizlabs.android.dbflow.annotation.Database
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.structure.ModelAdapter
import net.juzabel.socialimagelist.data.entity.TagEntity
import javax.inject.Inject


/**
 * Created by juzabel on 24/7/17.
 */
@Database(name = DbHelper.NAME, version = DbHelper.VERSION)
class DbHelper @Inject constructor(){

    companion object {
        const val NAME = "SocialImageListDB"

        const val VERSION = 1
    }

    fun getTagEntityModelAdapter() : ModelAdapter<TagEntity>{
            return FlowManager.getModelAdapter<TagEntity>(TagEntity::class.java);
    }



}