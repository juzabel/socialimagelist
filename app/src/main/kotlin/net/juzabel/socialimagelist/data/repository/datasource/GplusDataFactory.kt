package net.juzabel.socialimagelist.data.repository.datasource

import net.juzabel.socialimagelist.data.network.RestService
import java.lang.UnsupportedOperationException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 23/7/17.
 */
@Singleton
class GplusDataFactory @Inject constructor(private val restService: RestService) {

    fun createCloudDataStore(): GPlusDataStore = GplusCloudDataStore(restService)


    fun createDBDataStore(): GPlusDataStore = throw UnsupportedOperationException()

}