package net.juzabel.socialimagelist.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by juzabel on 23/7/17.
 */
data class GPlusItemEntity(
        @SerializedName("kind")
        @Expose
        var kind: String,

        @SerializedName("etag")
        @Expose
        var etag: String,

        @SerializedName("objectType")
        @Expose
        var objectType: String,

        @SerializedName("id")
        @Expose
        var id: String,

        @SerializedName("displayName")
        @Expose
        var displayName: String,

        @SerializedName("url")
        @Expose
        var url: String,

        @SerializedName("image")
        @Expose
        var imageEntity: GPlusImageEntity


)
