package net.juzabel.socialimagelist.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by juzabel on 23/7/17.
 */
data class GPlusPersonEntity(

    @SerializedName("kind")
    @Expose
    var kind : String,

    @SerializedName("etag")
    @Expose
    var etag : String,

    @SerializedName("selfLink")
    @Expose
    var selfLink : String,

    @SerializedName("title")
    @Expose
    var title : String,

    @SerializedName("nextPageToken")
    @Expose
    var nextPageToken : String,

    @SerializedName("image")
    @Expose
    var image : GPlusImageEntity
)
