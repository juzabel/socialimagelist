package net.juzabel.socialimagelist.data.repository.datasource

import net.juzabel.socialimagelist.data.cache.DbHelper
import java.lang.UnsupportedOperationException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 24/7/17.
 */
@Singleton
class TagDataFactory @Inject constructor(val dbHelper: DbHelper) {

    fun createCloudDataStore(): TagDataStore  = throw UnsupportedOperationException()

    fun createDBDataStore(): TagDataStore = TagDBDataStore(dbHelper)

}