package net.juzabel.socialimagelist.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by juzabel on 25/7/17.
 */
data class InstagramToken(
        @SerializedName("access_token")
        @Expose
        var accessToken: String
)