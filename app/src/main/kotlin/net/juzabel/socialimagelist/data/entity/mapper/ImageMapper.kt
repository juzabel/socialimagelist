package net.juzabel.socialimagelist.data.entity.mapper

import net.juzabel.socialimagelist.data.entity.GPlusEntity
import net.juzabel.socialimagelist.domain.Image
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 23/7/17.
 */
@Singleton
class ImageMapper @Inject constructor() {
    fun transform(entity: GPlusEntity): List<Image> {
        var listImages: ArrayList<Image> = ArrayList()

        for (i in 0..entity.items!!.size-1) {
            if(entity.items!!.get(i).image != null)
                listImages.add(Image(entity!!.items!!.get(i).image.url, "gplus"))


        }

        return listImages
    }

    fun transform(entity: String?): List<Image> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}