package net.juzabel.socialimagelist.presentation.ui.views.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_tag_list.*
import net.juzabel.socialimagelist.R
import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.presentation.SocialImageApplication
import net.juzabel.socialimagelist.presentation.di.components.DaggerFragmentComponent
import net.juzabel.socialimagelist.presentation.di.components.FragmentComponent
import net.juzabel.socialimagelist.presentation.di.modules.FragmentModule
import net.juzabel.socialimagelist.presentation.ui.contracts.TagListContract
import net.juzabel.socialimagelist.presentation.ui.presenters.TagListPresenter
import net.juzabel.socialimagelist.presentation.ui.views.adapters.TagListAdapter
import javax.inject.Inject

/**
 * Created by juzabel on 24/7/17.
 */
class TagListFragment : Fragment(), TagListContract.View {

    @Inject
    lateinit var tagListPresenter: TagListPresenter

    private var component: FragmentComponent? = null

    private lateinit var adapter : RecyclerView.Adapter<*>
    private lateinit var layoutManager : RecyclerView.LayoutManager

    private var tagList : List<Tag>? = null

    companion object {
        fun newInstance() = TagListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        component = DaggerFragmentComponent.builder()
                .applicationComponent((activity.application as SocialImageApplication).component)
                .fragmentModule(FragmentModule(this))
                .build()

        component!!.inject(this)
        tagListPresenter.view = this
        tagListPresenter.create()
        return inflater!!.inflate(R.layout.fragment_tag_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layoutManager = LinearLayoutManager(activity)
        fragment_tag_list_recyclerview.layoutManager = layoutManager;

        tagListPresenter.getList()
    }

    override fun error(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        //TODO Change into snackbar
    }

    override fun queryDone(tagList: List<Tag>) {
        this.tagList = tagList
        adapter  = TagListAdapter(tagList, context)
        fragment_tag_list_recyclerview.adapter = adapter

    }

    fun hasTagList(): Boolean {
        return tagList == null
    }
}