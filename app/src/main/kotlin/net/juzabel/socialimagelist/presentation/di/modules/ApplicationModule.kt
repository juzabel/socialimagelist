package net.juzabel.socialimagelist.presentation.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import net.juzabel.socialimagelist.data.executor.JobExecutor
import net.juzabel.socialimagelist.data.repository.ImageListDataRepository
import net.juzabel.socialimagelist.data.repository.TagDataRepository
import net.juzabel.socialimagelist.domain.executor.ExecutionThread
import net.juzabel.socialimagelist.domain.executor.PostExecutionThread
import net.juzabel.socialimagelist.domain.repository.ImageListRepository
import net.juzabel.socialimagelist.domain.repository.TagListRepository
import net.juzabel.socialimagelist.presentation.SocialImageApplication
import net.juzabel.socialimagelist.presentation.UIThread
import javax.inject.Singleton

/**
 * Created by juzabel on 21/7/17.
 */
@Module
class ApplicationModule(private val socialImageApplication: SocialImageApplication) {

    @Provides
    @Singleton
    fun socialImageApplication(): SocialImageApplication
            = socialImageApplication

    @Provides
    @Singleton
    fun provideContext(): Context
            = socialImageApplication

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ExecutionThread
            = jobExecutor


    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread
            = uiThread


    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences
            = socialImageApplication.getSharedPreferences("app", Context.MODE_APPEND)


    @Provides
    @Singleton
    fun provideImageListRepository(imageListRepository: ImageListDataRepository): ImageListRepository
            = imageListRepository


    @Provides
    @Singleton
    fun provideTagListRepository(tagListRepository: TagDataRepository): TagListRepository
            = tagListRepository
}