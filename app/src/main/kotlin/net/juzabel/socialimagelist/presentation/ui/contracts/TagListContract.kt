package net.juzabel.socialimagelist.presentation.ui.contracts

import net.juzabel.socialimagelist.domain.Tag

/**
 * Created by juzabel on 24/7/17.
 */
interface TagListContract {
    interface View {
        fun queryDone(tagList : List<Tag>)
        fun error(message : String)
    }
    interface Presenter {
        fun getList()
    }
}