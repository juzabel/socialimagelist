package net.juzabel.socialimagelist.presentation.ui.views.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import net.juzabel.socialimagelist.R
import net.juzabel.socialimagelist.presentation.ui.views.fragments.ImageListFragment
import net.juzabel.socialimagelist.presentation.ui.views.fragments.TagListFragment

/**
 * Created by juzabel on 25/7/17.
 */
class PagerAdapter(var context : Context, fragmentManager : FragmentManager) : FragmentStatePagerAdapter(fragmentManager){

    companion object {
        const val NUM_FRAGMENTS : Int = 2
    }

    val listFragment : ArrayList<Fragment?> = ArrayList()

    init {
        listFragment.add(ImageListFragment.newInstance())
        listFragment.add(TagListFragment.newInstance())

    }

    override fun getItem(position: Int): Fragment {
        return listFragment[position]!!
    }

    override fun getCount(): Int {
        return NUM_FRAGMENTS
    }

    override fun getPageTitle(position: Int): CharSequence {

        when(position){
            0 ->  return context.getString(R.string.fragment_images_name)
            1 -> return context.getString(R.string.fragment_tags_name)
        }
        return ""
    }
}