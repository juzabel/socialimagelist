package net.juzabel.socialimagelist.presentation.ui.presenters

import io.reactivex.observers.DisposableObserver
import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.domain.interactors.TagListInteractor
import net.juzabel.socialimagelist.presentation.ui.contracts.TagListContract
import javax.inject.Inject

/**
 * Created by juzabel on 24/7/17.
 */
open class TagListPresenter @Inject constructor(var tagListInteractor: TagListInteractor): TagListContract.Presenter{
    var view: TagListContract.View? = null

    fun create() {
    }

    override fun getList() {
        tagListInteractor.execute(TagListObserver())

    }

     inner class TagListObserver : DisposableObserver<List<Tag>>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            view!!.error(e.message!!)
        }

        override fun onNext(tagList: List<Tag>) {
            view!!.queryDone(tagList)
        }
    }
}