package net.juzabel.socialimagelist.presentation.ui.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import net.juzabel.socialimagelist.R
import net.juzabel.socialimagelist.domain.Tag


/**
 * Created by juzabel on 25/7/17.
 */
class TagListAdapter constructor(var tagList: List<Tag>, var context : Context) : RecyclerView.Adapter<TagListAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return tagList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {

        var view: View = LayoutInflater.from(parent!!.getContext())
                .inflate(R.layout.adapter_tag_list_item, parent, false)

        var viewHolder: ViewHolder = ViewHolder(view)

        return viewHolder

    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder!!.tag!!.text = tagList[position].name

    }


    /**
     * ViewHolder
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tag : TextView? = null

        init {
            tag = view.findViewById(R.id.tag_textview) as TextView?
        }
    }

}