package net.juzabel.socialimagelist.presentation

import android.app.Application
import android.content.Context
import com.raizlabs.android.dbflow.config.FlowManager
import net.juzabel.socialimagelist.presentation.di.components.ApplicationComponent
import net.juzabel.socialimagelist.presentation.di.components.DaggerApplicationComponent
import net.juzabel.socialimagelist.presentation.di.modules.ApplicationModule

/**
 * Created by juzabel on 21/7/17.
 */
class SocialImageApplication : Application() {
    val component: ApplicationComponent
        get() = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
        FlowManager.init(this);
    }

    companion object {

        operator fun get(context: Context): SocialImageApplication {
            return context.applicationContext as SocialImageApplication
        }
    }
}