package net.juzabel.socialimagelist.presentation.ui.views.activities

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import net.juzabel.socialimagelist.R
import net.juzabel.socialimagelist.presentation.ui.views.adapters.PagerAdapter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val fragTransaction = supportFragmentManager.beginTransaction()
//
//        val myFrag = ImageListFragment.newInstance()
//
//        fragTransaction.add(fragment_container.id, myFrag, "fragment")
//        fragTransaction.commit()

        setSupportActionBar(toolbar)
        viewpager.adapter = PagerAdapter(this, supportFragmentManager)
        tab_layout.setupWithViewPager(viewpager)


    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)


    }
}
