package net.juzabel.socialimagelist.presentation.di.scopes

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

/**
 * Created by juzabel on 21/7/17.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class FragmentScope
