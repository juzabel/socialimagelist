package net.juzabel.socialimagelist.presentation.ui.presenters

import io.reactivex.observers.DisposableObserver
import net.juzabel.socialimagelist.domain.Image
import net.juzabel.socialimagelist.domain.Tag
import net.juzabel.socialimagelist.domain.interactors.ImageListInteractor
import net.juzabel.socialimagelist.domain.interactors.TagInteractor
import net.juzabel.socialimagelist.presentation.di.scopes.FragmentScope
import net.juzabel.socialimagelist.presentation.ui.contracts.ImageListContract
import javax.inject.Inject


/**
 * Created by juzabel on 21/7/17.
 */
@FragmentScope
class ImageListPresenter @Inject
constructor(private val imageListInteractor: ImageListInteractor, private val tagInteractor: TagInteractor) : ImageListContract.Presenter {

    var view: ImageListContract.View? = null


    fun create() {

    }

    override fun search(tag: String) {
        imageListInteractor.setParameters(tag, 1, 30)
        imageListInteractor.execute(UserListObserver())

        tagInteractor.setParameters(tag)
        tagInteractor.execute(TagObserver())
    }

    private inner class UserListObserver : DisposableObserver<List<Image>>() {

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            if(view != null && e != null && e.message!=null) {
                view!!.error(e.message!!)
            }
            e.printStackTrace()

        }

        override fun onNext(imageList: List<Image>) {
            view!!.searchDone(imageList)
        }
    }

    private inner class TagObserver : DisposableObserver<Tag>() {

        override fun onNext(t: Tag?) {
            view!!.searchSaved(t!!.name)

        }

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

            view!!.error(e.message!!)
        }
    }
}