package net.juzabel.socialimagelist.presentation.di.components

import dagger.Component
import net.juzabel.socialimagelist.presentation.di.modules.FragmentModule
import net.juzabel.socialimagelist.presentation.di.scopes.FragmentScope
import net.juzabel.socialimagelist.presentation.ui.views.fragments.ImageListFragment
import net.juzabel.socialimagelist.presentation.ui.views.fragments.TagListFragment

/**
 * Created by juzabel on 21/7/17.
 */
@FragmentScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
    fun inject(imageListFragment: ImageListFragment)
    fun inject(tagListFragment: TagListFragment)

}