package net.juzabel.socialimagelist.presentation.di.modules

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import net.juzabel.socialimagelist.presentation.di.scopes.FragmentScope

/**
 * Created by juzabel on 21/7/17.
 */
@Module
class FragmentModule(private val baseFragment: Fragment) {

    @Provides
    @FragmentScope
    fun provideFragment(): Fragment = this.baseFragment

}
