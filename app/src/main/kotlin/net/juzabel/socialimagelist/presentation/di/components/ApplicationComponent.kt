package net.juzabel.socialimagelist.presentation.di.components

import android.content.Context
import dagger.Component
import net.juzabel.socialimagelist.domain.executor.ExecutionThread
import net.juzabel.socialimagelist.domain.executor.PostExecutionThread
import net.juzabel.socialimagelist.domain.repository.ImageListRepository
import net.juzabel.socialimagelist.domain.repository.TagListRepository
import net.juzabel.socialimagelist.presentation.SocialImageApplication
import net.juzabel.socialimagelist.presentation.di.modules.ApplicationModule
import net.juzabel.socialimagelist.presentation.navigation.Navigator
import javax.inject.Singleton

/**
 * Created by juzabel on 21/7/17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(socialImageApplication: SocialImageApplication)

    val socialImageApplication : SocialImageApplication

    fun context() : Context

    fun navigator() : Navigator

    fun threadExecutor(): ExecutionThread

    fun postExecutionThread(): PostExecutionThread

    fun imageListRepository(): ImageListRepository

    fun tagListRepository(): TagListRepository
}