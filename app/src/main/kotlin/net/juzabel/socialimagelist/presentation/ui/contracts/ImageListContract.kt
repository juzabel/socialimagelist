package net.juzabel.socialimagelist.presentation.ui.contracts

import net.juzabel.socialimagelist.domain.Image

/**
 * Created by juzabel on 21/7/17.
 */
interface ImageListContract {
    interface View {
        fun searchDone(imageList : List<Image>)
        fun error(message : String)
        fun searchSaved(name: String)
    }
    interface Presenter {
        fun search(tag : String)
    }
}