package net.juzabel.socialimagelist.presentation.ui.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import net.juzabel.socialimagelist.R
import net.juzabel.socialimagelist.domain.Image


/**
 * Created by juzabel on 25/7/17.
 */
class ImagesAdapter constructor(var imageList: List<Image>, var context : Context) : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return imageList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {

        var view: View = LayoutInflater.from(parent!!.getContext())
                .inflate(R.layout.adapter_image_list_item, parent, false)

        var viewHolder: ViewHolder = ViewHolder(view)

        return viewHolder

    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        Glide.with(context).load(imageList[position].url).into(holder!!.image)

        holder.platform!!.text = imageList[position].socialNetwork

    }


    /**
     * ViewHolder
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
         var image: ImageView? = null
         var platform: TextView? = null

        init {
            image = view.findViewById(R.id.image) as ImageView?
            platform = view.findViewById(R.id.platform) as TextView?
        }
    }

}