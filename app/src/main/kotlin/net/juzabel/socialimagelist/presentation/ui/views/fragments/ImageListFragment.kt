package net.juzabel.socialimagelist.presentation.ui.views.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.fragment_image_list.*
import net.juzabel.socialimagelist.R
import net.juzabel.socialimagelist.domain.Image
import net.juzabel.socialimagelist.presentation.SocialImageApplication
import net.juzabel.socialimagelist.presentation.di.components.DaggerFragmentComponent
import net.juzabel.socialimagelist.presentation.di.components.FragmentComponent
import net.juzabel.socialimagelist.presentation.di.modules.FragmentModule
import net.juzabel.socialimagelist.presentation.ui.contracts.ImageListContract
import net.juzabel.socialimagelist.presentation.ui.presenters.ImageListPresenter
import net.juzabel.socialimagelist.presentation.ui.views.adapters.ImagesAdapter
import java.util.*
import javax.inject.Inject


/**
 * Created by juzabel on 21/7/17.
 */
class ImageListFragment : Fragment(), ImageListContract.View, MaterialSearchView.OnQueryTextListener {

    companion object {
        fun newInstance() = ImageListFragment()
        const val IMAGE_LIST = "IMAGE_LIST"
    }

    @Inject
    lateinit var imageListPresenter: ImageListPresenter

    private var component: FragmentComponent? = null

    private lateinit var adapter: RecyclerView.Adapter<*>
    private lateinit var layoutManager: RecyclerView.LayoutManager

    private var imageList: List<Image>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        component = DaggerFragmentComponent.builder()
                .applicationComponent((activity.application as SocialImageApplication).component)
                .fragmentModule(FragmentModule(this))
                .build()

        component!!.inject(this)
        imageListPresenter.view = this
        imageListPresenter.create()

        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState)
        }
        return inflater!!.inflate(R.layout.fragment_image_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (imageList != null) {
            setAdapter(imageList!!)
        }
        edittext_search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                doSearch()

                return@OnEditorActionListener true
            }
            false
        })

        search_imagebutton.setOnClickListener {
            doSearch()
        }


        fragment_image_list_recyclerview.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(activity)
        fragment_image_list_recyclerview.layoutManager = layoutManager;


    }

    private fun doSearch() {
        hideKeyboard()
        imageListPresenter.search(edittext_search.text.trim().toString())

    }

    private fun hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(edittext_search.applicationWindowToken, 0)
    }

    override fun searchDone(imageList: List<Image>) {

        this.imageList = imageList

        setAdapter(imageList)
    }

    private fun setAdapter(imageList: List<Image>) {
        adapter = ImagesAdapter(imageList, context)
        fragment_image_list_recyclerview.adapter = adapter
    }

    override fun searchSaved(name: String) {
        Toast.makeText(context, "Saved: " + name, Toast.LENGTH_LONG).show()

    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        imageListPresenter.search(query!!)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        //Nothing to do
        return false
    }

    override fun error(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        //TODO Change into snackbar
    }

    fun onRestoreInstanceState(inState: Bundle?) {
        imageList = inState!!.getParcelableArrayList(IMAGE_LIST)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (imageList == null) {
            outState!!.putParcelableArrayList(IMAGE_LIST, null)
        } else {
            outState!!.putParcelableArrayList(IMAGE_LIST, imageList as ArrayList<Image>)
        }

    }
}