package net.juzabel.socialimagelist.presentation

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import net.juzabel.socialimagelist.domain.executor.PostExecutionThread
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by juzabel on 21/7/17.
 */
@Singleton
class UIThread
@Inject
constructor() : PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}